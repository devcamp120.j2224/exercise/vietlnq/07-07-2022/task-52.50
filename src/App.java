import java.util.ArrayList;

import java.util.Date;

import com.devcamp.s50.Order;

public class App {
    public static void main(String[] args) throws Exception {
        ArrayList<Order> orderList = new ArrayList<Order>();
        Order order1 = new Order();
        orderList.add(order1);

        Order order2 = new Order(2, "Viet Dep Trai", 30000);
        orderList.add(order2);
        
        Order order3 = new Order(3, "Viet Xau Trai", 30000, new Date());
        orderList.add(order3);
        
        Order order4 = new Order(3, "Viet Hung Du", 70000, new Date(), true, new String[] {"pizza", "suon nuong"});
        orderList.add(order4);
        
        for (Order order: orderList) {
            System.out.println(order.toString());
        }
    }
}
