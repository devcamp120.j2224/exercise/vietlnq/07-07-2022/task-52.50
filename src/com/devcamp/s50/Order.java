package com.devcamp.s50;

import java.text.NumberFormat;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;

public class Order {
    public int id;
    public String customerName;
    public long price;
    public Date orderDate;
    public boolean confirm;
    public String[] items;

    // 1.
    public Order() {
        this.id = 1;
        this.customerName = "Viet";
        this.price = 20000;
        this.orderDate = new Date();
        this.confirm = false;
        this.items = new String[] {"pizza", "nuoc ngot"};
    }

    // 2.
    public Order(int id, String name, long price) {
        this.id = id;
        this.customerName = name;
        this.price = price;
    }

    // 3. 
    public Order(int id, String name, long price, Date date) {
        this(id, name, price);
        this.orderDate = date;
    }

    // 4. 
    public Order(int id, String name, long price, Date date, boolean confirm, String[] items) {
        this(id, name, price, date);
        this.items = items;
    }

    // Hàm hiển thị ra terminal:
    public void showOrder() {
        System.out.println("ID: " + this.id);
        System.out.println("Name: " + this.customerName);
        System.out.println("Price: " + this.price);
        System.out.println("Order Date: " + this.orderDate);
        System.out.println("Confirm: " + this.confirm);
        System.out.println("items: " + this.items);
    }

    @Override
    public String toString() {
        String formattedDate = null;
        String formattedPrice = "N/A";

        if (this.orderDate != null) {
            Locale.setDefault(new Locale("vi", "VN"));
            String dateTimePattern = "dd-MMMM-yy HH:mm:ss.SSS";
            DateTimeFormatter defaulTimeFormatter = DateTimeFormatter.ofPattern(dateTimePattern);
            
            formattedDate = defaulTimeFormatter.format(this.orderDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime());
        }

        if (this.price != 0) {
            // Định dạng giá tiền:
            Locale usLocale = Locale.getDefault();
            NumberFormat usNumberFormat = NumberFormat.getCurrencyInstance(usLocale);

            formattedPrice = usNumberFormat.format(price);
        }

        String vResult = "Order [id= " + this.id
            + ", customerName= " + this.customerName
            + ", price=" + formattedPrice
            + ", orderDate=" + formattedDate
            + ", confirm= " + this.confirm
            + ", items= " + this.items;

        return vResult;
        
    }

}
